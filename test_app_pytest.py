import pytest
from app import TaxRate
from random import choice

t = TaxRate()

class TestTax():

    # test for salary < 4000$. Tax free
    def test_tax_free(self):
        x = choice(range(3999))
        expected_result = x
        assert expected_result == t.tax_value(x)

    # test for salary 4000 < x < 5500. Tax = 10%
    def test_tax_10(self):
        x = choice(range(4000,5499))
        expected_result = x*0.1
        assert expected_result == t.tax_value(x)

    # test for salary 5500 < x < 33500. Tax = 22%
    def test_tax_22(self):
        x = choice(range(5500, 33500))
        expected_result = x * 0.22
        assert expected_result == t.tax_value(x)

    # test for salary x > 33500. Tax = 40%
    def test_tax_40(self):
        x = choice(range(33500, 1000000))
        expected_result = x * 0.4
        assert expected_result == t.tax_value(x)
