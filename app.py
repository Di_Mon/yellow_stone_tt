class TaxRate():

    def tax_value(self, x):
        if x < 4000:
            return x
        elif 4000 < x < 5500:
            return x*0.1
        elif 5500 < x < 33500:
            return x*0.22
        elif x > 33500:
            return x*0.4